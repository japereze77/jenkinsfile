
import org.jenkinsci.plugins.pipeline.modeldefinition.Utils
def path = "wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json"

def stageMain (){

switch(env.gitlabSourceBranch) {
  case "Test":

      //stageMongo()
      stageAmbiente()
      stageConstruccion()
      stagePruebas()
      stageAlmacenado()
      stageDespliegue()
      stageReverso()

  break
  case "QA":
    stageAmbiente()
    stageConstruccion()
    stagePruebas()
    stageAutorizacionDespliegue()
    stageAlmacenado()
    stageDespliegue()
    stageAutorizacionFinal()
    stagePostDespliegue()
    stageReverso()

  break
}


}//Fin Main


def stageMongo(){

    //Stage encargado de realizar la conexión a una base de datos MongoDB
    //Esto con el fin de guardar el proceso del pipeline.
      stage("Mongo"){
          wrap([$class: 'AnsiColorBuildWrapper']) {
                  echo "\u001B[31m*********** Entrando a Mongo\u001B[m"
          }
          if (env.gitlabSourceBranch!=null) {
                  def dataError = load "${pwd()}@script/modelo/errores.groovy"
                  def conexion = load "${pwd()}@script/controlador/conexion.groovy"
                  println "Validando error"
                  ambiente = dataError.error(conexion.inicializarMongo())
                  ambiente = dataError.error(conexion.insertarMongo())
                  println "Ambiente Error : " + ambiente
                  if (ambiente!=null){
                          currentBuild.result = 'ABORTED'
                          error(ambiente)
                  }
                  }else{
                          Utils.markStageSkippedForConditional(STAGE_NAME)
                  }
    }
}// Fin Mongo

    def stageAmbiente(){

      stage('Ambiente') {
        //Stage encargado de crear un ambiente en el workspace donde el principal uso es el generar
        //un archivo json para verificar el  status del pipeline así como el log de compilación.
              wrap([$class: 'AnsiColorBuildWrapper']) {
                    echo "\u001B[31m*********** Entrando al Ambiente\u001B[m"
              }
              println "Ambiente :" +env.gitlabSourceBranch
              if (env.gitlabSourceBranch!=null) {
                      def dataError = load "${pwd()}@script/modelo/errores.groovy"
                      def ambienteJson = load "${pwd()}@script/controlador/ambiente.groovy"
                      ambiente = dataError.error(ambienteJson.ambienteJson("wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json"))
                      println "Ambiente Error : " + ambiente
                      if (ambiente!=null){
                              currentBuild.result = 'ABORTED'
                              error(ambiente)
                      }
              }else{
                  Utils.markStageSkippedForConditional(STAGE_NAME)
              }
        }

    }// Fin Ambiente

    def stageConstruccion(){

      stage('Construcción') {
        //Stage generado para realizar la construcción de los artiefactos tanto en snapshot como candidate release
        //y el artefacto productivo el cual es un release todo esto con base a las configuraciones del archivo
        // Build.gradle.
              wrap([$class: 'AnsiColorBuildWrapper']) {
                    echo "\u001B[31m*********** Entrando a Construcción\u001B[m"
              }

         if (env.gitlabSourceBranch!=null) {
                 def dataError = load "${pwd()}@script/modelo/errores.groovy"
                 def construccion = load "${pwd()}@script/controlador/construccion.groovy"
                 def ambiente = dataError.error(construccion.compilar("wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json"))
                 println "Construcción Error : "+ambiente
                 if (ambiente!=null){
                         currentBuild.result = 'ABORTED'
                         error(ambiente)
                 }
         }else{
             Utils.markStageSkippedForConditional(STAGE_NAME)
         }

      }
    }//Fin Construcción

    def stagePruebas(){

      stage('Pruebas'){
      //Stage encargado de realizar las pruebas conforme a la versión del ambiente que es generado
      //en el pipeline Fortify, Checkmarks entre otros.
              wrap([$class: 'AnsiColorBuildWrapper']) {
                  echo "\u001B[31m*********** Entrando a Pruebas\u001B[m"
              }
          if (env.gitlabSourceBranch!=null) {
              def test = load "${pwd()}@script/controlador/test.groovy"
              //test.fortify()
        }else{
            Utils.markStageSkippedForConditional(STAGE_NAME)
        }
      }
   }//Fin Pruebas

   def stageAutorizacionDespliegue(){

      stage('Autorización Despliegue'){
      //Autoriazación despliegue encargado enviar por correo electronico la posible autorización o rechado para
      //continuar con el pipeline.
              wrap([$class: 'AnsiColorBuildWrapper']) {
                  echo "\u001B[31m*********** Entrando a Autorización Despliegue\u001B[m"
              }
              def dataError = load "${pwd()}@script/modelo/errores.groovy"
              def autorizacion = load "${pwd()}@script/controlador/autorizacion.groovy"
            if (env.gitlabSourceBranch=='QA') {
                  def ambiente = dataError.error(autorizacion.autorizacion("wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json"))
                  println "Autorización Error : "+ambiente

                  if (ambiente!=null){
                          currentBuild.result = 'ABORTED'
                          error(ambiente)
                          }
            }else{
              Utils.markStageSkippedForConditional(STAGE_NAME)
            }
      }
    }//Fin Autorización Despliegue

   def stageAlmacenado(){

      stage('Almacenado'){
      //Stage encargado de almacenar en un repositorio de artefactos los artefactos que son requeridos en sus
      // distintos ambientes.
              wrap([$class: 'AnsiColorBuildWrapper']) {
                   echo "\u001B[31m*********** Entrando al Almacenado : ${params.reverso}\u001B[m"
              }
              if (env.gitlabSourceBranch!=null){
                      def dataError = load "${pwd()}@script/modelo/errores.groovy"
                      def envioArtifactory = load "${pwd()}@script/controlador/artifactory.groovy"
                      envioArtifactory.orquestadorArtifactory("envio","wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json")
              }else{
                  Utils.markStageSkippedForConditional(STAGE_NAME)
              }
      }
  }//Fin Almacenado

  def stageDespliegue(){
      stage('Despliegue') {
        //Stage encargado de realizar el  deploy en  desarrollo y QA en los servidores 139 y 179
              wrap([$class: 'AnsiColorBuildWrapper']) {
                              echo "\u001B[31m*********** Entrando a Despliegue\u001B[m"
              }
              if (!params.reverso){
                def dataError = load "${pwd()}@script/modelo/errores.groovy"
                def descargaArtifactory = load "${pwd()}@script/controlador/artifactory.groovy"
                descargaArtifactory.orquestadorArtifactory('descarga',"wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json")
                def deploy = load "${pwd()}@script/controlador/deploy.groovy"
                      if (env.gitlabSourceBranch == "QA"){

                        //deploy.calculoIPAmbiente()

                      }else if(env.gitlabSourceBranch == "Test") {
                        //deploy.enviarSSHDeploy("Test","Estatus_Deploy_10_63_32_179","10.63.32.179")
                      }
              }else{
                  Utils.markStageSkippedForConditional(STAGE_NAME)
              }

      }
  }//FinDespliegue

  def stageAutorizacionFinal(){

      stage('Autorización Final'){
      //Este Stage se encuentra encargado de dar paso al siguiente ambiente que por orden jerarquica
      // se encuantra de la siguiente manera Test -> QA y por ultimo master.
              wrap([$class: 'AnsiColorBuildWrapper']) {
                              echo "\u001B[31m*********** Entrando a Autorización Final\u001B[m"
              }
      }
  } //FinAlmacenado

  def stagePostDespliegue(){
      stage('Post-Despliegue') {
      //Este stage se encarga de realizar todas las adaptaciones que son enviadas despues del depligue como el envio
      // de los cambios al área de monitoreo.
              wrap([$class: 'AnsiColorBuildWrapper']) {
                              echo "\u001B[31m*********** Entrando a Post-Despliegue\u001B[m"
              }
              if (env.gitlabSourceBranch=='QA') {
                  def dataError = load "${pwd()}@script/modelo/errores.groovy"
                  def postDeploy = load "${pwd()}@script/controlador/postDeploy.groovy"
                  def ambiente = dataError.error(postDeploy.enviarCambioJson())
                  println "PostDeploy Error : "+ambiente
                  if (ambiente!=null){
                          currentBuild.result = 'ABORTED'
                          error(ambiente)
                        }
              }else{
                  Utils.markStageSkippedForConditional(STAGE_NAME)
              }

      }
  }//Fin Post Despliegue

 def stageReverso(){

      stage('Autorización Reverso'){
        //Stage Reverso se encarga se ser necesario realizar un reverso del artefacto que se encuentra deployado por uno
        // de versión estable.
              wrap([$class: 'AnsiColorBuildWrapper']) {
                              echo "\u001B[31m*********** Entrando a Autorización Reverso\u001B[m"
              }
              def dataError = load "${pwd()}@script/modelo/errores.groovy"
              def autorizacion = load "${pwd()}@script/controlador/autorizacion.groovy"
              def ambiente = dataError.error(autorizacion.autorizacion("wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json"))
              println "Autorización Error : "+ambiente
              if (ambiente!=null){
                      currentBuild.result = 'ABORTED'
                      error(ambiente)
              }
      }
  }// Fin Reverso


return this

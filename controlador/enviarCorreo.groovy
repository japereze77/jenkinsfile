import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import jenkins.*
import jenkins.model.*
import hudson.*
import hudson.model.*
import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import com.cloudbees.groovy.cps.NonCPS

/*
@Creado: José Arturo Pérez
@version: 1.0
*/

@NonCPS
def nombreArtefacto() {
  try{
    println "==== ENTRANDO A CREAR URL ===="
    def nodes = Jenkins.getInstance().getGlobalNodeProperties()
    nodes.getAll(hudson.slaves.EnvironmentVariablesNodeProperty.class)

    println "DEFINIENDO VARIABLE"
    if(false){
      println "== ERROR =="
      println("error: unexpected number of environment variable containers: " + nodes.size() + " expected: 1")
    } else {

      println "ENTRANDO AL ELSE"
      def envVars = nodes.get(0).getEnvVars()
      println "=== 0"
      def ruta = "wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json"
      println "Ruta: ${ruta}"
      println "=== 1"
      def jsonConstruccion = readFile(file:''+ruta)
      println "=== 2"
      def dataConstruccion = new JsonSlurperClassic().parseText(jsonConstruccion)
      println "=== 3"
      def urlEnvio = dataConstruccion.cambio.url_artefacto
      println "=== 4"
      //println "URL CREADA ES: ${urlEnvio}"
      //envVars.delete("URL_ENVIO")
      envVars.put("URL_ENVIO", "${urlEnvio}")
      Jenkins.getInstance().save()
      println("Variable de la URL Creada")
    }


  }catch(e){
    println "ERROR AL CREAR LA VARIBLE DE ENTORNO PARA URL DEL ARTEFACTO"
    println e
  }
}

def envioEmail (emailStatus,fallido) {

    println "---| Enviando Email | ---"
    def status = true
    def rootDir = pwd()
    //def buildLog = "wsBancaDigitialJson_${BUILD_NUMBER}/log.txt"
    def buildLog = "wsBancaDigitalJson_${BUILD_NUMBER}/log.txt"

  //def mailsBanco = "cortizs@bancoazteca.com.mx,jcperezor@elektra.com.mx,erick.morales@elektra.com.mx,japereze@elektra.com.mx,adiazt@elektra.com.mx,megarcial@elektra.com.mx,amoralesl@elektra.com.mx,ajcruz@elektra.com.mx,hvargase@elektra.com.mx,eluz@elektra.com.mx,jesanchezga@elektra.com.mx,lcorread@elektra.com.mx,german.torres@elektra.com.mx,ivan.nabor@elektra.com.mx,pedro.angeles@elektra.com.mx,ricardo.cruz@elektra.com.mx"
  //def mailsFortify = "lcorread@elektra.com.mx, csa-ekt@elektra.com.mx"
  //def mailOrigin = "superDevOps@bancoazteca.com.mx"

  escribirLog()

  switch(emailStatus) {

          case 'sendAutoriza' :
          println "CORREO DE AUTORIZACION"
          wrap([$class: 'BuildUser']) {
                emailext attachmentsPattern: buildLog,
                subject: "Autorizacion de despliegue",
                body:  new File ("${rootDir}@script/partials/autorizacion1.html").text,
                to:   'japereze@elektra.com.mx',
                from: 'japereze@elektra.com.mx'
              }
          break;

          case 'construccion' :
          //Envía notificación vía correo electrónico indicando sobre el status de la construcción del artefacto en los diferentes ambientes
          escribirLog()
          println fallido? "---| Email -> Construcción Failed |--- "+env.gitlabSourceBranch : "---|  Email Construcción -> Construcción Exitosa |--- "+env.gitlabSourceBranch
          wrap([$class: 'BuildUser']) {
            emailext attachmentsPattern: buildLog,
            subject: fallido? "Construcción Fallida  - WAR Banca Digital Móvil / "+env.gitlabSourceBranch : "Construcción Exitosa  - WAR Banca Digital Móvil /"+env.gitlabSourceBranch,
            body:  new File (fallido? "${rootDir}@script/partials/_construccion/construccionFallida.html" : "${rootDir}@script/partials/_construccion/construccionSuccess.html").text,
            to:   'japereze@elektra.com.mx',
            from: 'japereze@elektra.com.mx'
          }
          break;

          case 'desployTest' :
          //Envía notificación vía correo electrónico indicando sobre el status del deploy  del artefacto en los diferentes ambientes
          nombreArtefacto()
          println fallido? "---| Email -> Despliegue Failed |--- "+ env.gitlabSourceBranch +"---|" : "---|  Email -> Despliegue Success |--- "+ env.gitlabSourceBranch +"---|"
          wrap([$class: 'BuildUser']) {
            emailext attachmentsPattern: buildLog,
            subject: fallido? "Despliegue Fallido  - WAR Banca Digital Móvil / "+env.gitlabSourceBranch : "Despliegue Exitoso  - WAR Banca Digital Móvil / "+env.gitlabSourceBranch,
            body: new File (fallido? "${rootDir}@script/partials/_despliegue/despliegueFallido.html" : "${rootDir}@script/partials/_despliegue/despliegueSuccess.html").text,
            to: 'jcperezor@elektra.com.mx',
            from: 'jcperezor@elektra.com.mx'
          }
          break;

          case 'fortifySuccess':
          escribirLog()
          println "---| Email -> Fority-Success |--- "env.gitlabSourceBranch

          wrap([$class: 'BuildUser']) {
            emailext attachmentsPattern: buildLog,
            subject: "Estatus Test - Fortify Terminado",
            body: new File (fallido? "${rootDir}@script/partials/_fortify/fortifyFallido.html" : "${rootDir}@script/partials/_fortify/fortifySuccess.html").text,
            to: 'jcperezor@elektra.com.mx',
            from: 'jcperezor@elektra.com.mx'
          }

          break;

          case 'cambiosJson':
          //Envía notificación vía correo electrónico indicando sobre el status del cambio que sera enviado a producción para su deploy al área de Monitoreo
          escribirLogJson()
          println fallido? "---| Email -> Envio de Cambio Fallido |--- "+ env.gitlabSourceBranch +" ---|" : "---|  Email -> Envio de cambio Exitoso |--- "+ env.gitlabSourceBranch +" ---|"

          wrap([$class: 'BuildUser']) {
            emailext attachmentsPattern: buildLog,
            subject: "Estatus de Cambio para Deploy",
            body: new File (fallido? "${rootDir}@script/partials/_soapJson/cambioJsonFallido.html" : "${rootDir}@script/partials/_soapJson/ambioJsonExitoso.html").text,
            to: 'japereze@elektra.com.mx',
            from: 'japereze@elektra.com.mx'
          }

            break;
            case 'autorizacionFallida':
            //Envía notificación vía correo electrónico Indicando que la autorización ha sido autorizada
            println "---| Email -> Envio Autorizacion ya usada |---"

            wrap([$class: 'BuildUser']) {
              emailext subject: "Ejecución ya Utilizada",
              body: new File ("${rootDir}@script/partials/_errores/autorizacionFallida.html").text,
              to: 'japereze@elektra.com.mx',
              from: 'japereze@elektra.com.mx'
            }
            break;
            case 'notificaReverso':

            println "---| Email -> Envio notif que se va a hacer un reverso |---"

            wrap([$class: 'BuildUser']) {
              emailext subject: "Notificacion de reverso",
              body: new File ("${rootDir}@script/partials/"+pagina+".html").text,
              to: 'ricardo.cruz@elektra.com.mx',
              from: 'ricardo.cruz@elektra.com.mx'
            }
            break;
          }
        }

        def crearFecha() {
          try{
            def nodes = Jenkins.getInstance().getGlobalNodeProperties()
            nodes.getAll(hudson.slaves.EnvironmentVariablesNodeProperty.class)

            if ( false) {
              println("error: unexpected number of environment variable containers: " + nodes.size() + " expected: 1")
              } else {
                def envVars = nodes.get(0).getEnvVars()
                envVars.put("FechaCompilacion", ""+new SimpleDateFormat("dd 'de' MMM 'de' yyyy 'a las' hh:mm aaaa", new Locale("es_ES")).format(new Date()))
                Jenkins.getInstance().save()
                println("Fecha Almacenada")
              }
              }catch(e){
                e.getMessage()
              }
            }

            def escribirLog(){
              try{
                // obtiene la salida del log y lo guarda en la variable logcon
                def logcon = currentBuild.rawBuild.getLog(10000)
                //la tura debe estar definida para poder alcanzarla
                def rutalog = "wsBancaDigitalJson_${BUILD_NUMBER}/log.txt"
                //println "buildLog = "+rutalog
                def texto = ""
                for(def i=0; i<logcon.size(); i++)
                {
                  texto = texto + logcon.get(i)+"\n"
                }
                //escribe en el archivo
                writeFile(file:''+rutalog, text: texto)

                }catch(e){
                  println"Exceptción|---- Error al escribir en el archivo"
                  println  e.getMessage()
                  }finally{

                  }
                }

                def escribirLogJson(){
                  try{

                    def rutalog = "wsBancaDigitalJson_${BUILD_NUMBER}/log.txt"
                    def texto = readFile  "wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json"

                    //escribe en el archivo
                    writeFile(file:''+rutalog, text: texto)

                    }catch(e){
                      println"Exceptción|---- Error al escribir en el archivo"
                      println  e.getMessage()
                      }finally{

                      }
                    }

return this

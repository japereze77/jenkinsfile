import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import hudson.model.*
import jenkins.model.Jenkins

/*
@Creado: José Arturo Pérez
@version: 1.0
*/

def orquestadorTest() {

  try {


    println "RAMA ORIGEN:"
    println gitlabSourceBranch

    switch(env.gitlabSourceBranch) {
      case 'Test':
        println "STAGE(Test) -> Rama TEST. Ejecutando pruebas de Fortify"
        fortify()

      break
      case 'QA':
        println "STAGE(Test) -> Rama QA. Ejecutando robot de pruebas de fortify"
        robotQA()

      break
    }

  } catch(e) {
    println "ERROR AL EJECUTAR ORQUESTADOR DE TEST"
    println e
  } finally {
    println "TERMINA EJECUCION DE ORQUESTADOR TEST"
  }

}

def fortify() {

  try{

      println "******************************* | Pruebas Fortify | ********************************************"

    sh "/opt/HP_Fortify/HP_Fortify_SCA_and_Apps_4.40/bin/sourceanalyzer -b ${JOB_NAME} -clean"
    sh "/opt/HP_Fortify/HP_Fortify_SCA_and_Apps_4.40/bin/sourceanalyzer -b ${JOB_NAME} -jdk '1.8' -cp '/build/libs/*.war' '/var/lib/jenkins/workspace/${JOB_NAME}/wsBancaDigital'"
    sh "/opt/HP_Fortify/HP_Fortify_SCA_and_Apps_4.40/bin/sourceanalyzer -b ${JOB_NAME} -scan -f '/var/lib/jenkins/workspace/${JOB_NAME}/Resultados.fpr'"

    println "Termina ejecucion de pruebas Fortify"


    println "Mandado archivo de pruebas Fortify a consolas administrativas de DSI"

    build job: 'Envio-Fortify-WSBancaDigital', parameters: [stringParam(name: 'rutaArchivo', value: "${JOB_NAME}")]
    println "Envio de archivo Fortify Terminado"

    // Notificacion Fortify
    def rootDir = pwd()
    def email = load "${rootDir}@script/controlador/enviarCorreo.groovy"
    email.envioEmail('fortifySuccess',false)

     //Escribiendo en Json
     //jsonConstruccion = JsonOutput.toJson(dataConstruccion)?:null
     //jsonConstruccion = JsonOutput.prettyPrint(jsonConstruccion)?:null
     //Colocando el cambio en el archivo
     //writeFile(file:''+path, text: jsonConstruccion)?:null



  } catch(e){
    println "******************* ERROR AL EJECUAR PRUEBAS FORTIFY *******************"
    println e

    // Notificacion Fortify
    def rootDir = pwd()
    def email = load "${rootDir}@script/controlador/enviarCorreo.groovy"
    email.envioEmail('fortifySuccess',true)

  } finally{

  }

}

def robotQA() {
    println "SERVICIO EN CONSTRUCCION, GRACIAS..."
}

return this

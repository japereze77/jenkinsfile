import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import groovy.io.FileType
import static groovy.io.FileType.*
import static groovy.io.FileVisitResult.*
import hudson.model.*
import jenkins.model.Jenkins

def rootDir = pwd()

/*
@Creado: José Arturo Pérez
@version: 1.0
*/


  def clonar(){
  println "******************************* | Construcción | ********************************************"

        try{
            println "*| Clonando repositorio | *"
            println "(1)-------| Clonando respositorio wsbancadigital Test..."
                              git branch: env.gitlabSourceBranch, credentialsId: 'gitlab', url: 'http://10.50.109.50/bazdigital_serviciosbdm/wsbancadigital.git'
            println "(2)-------| Clonado wsbancadigital Test Exitso..."
        }catch(e){

        }finally{

        }
 }

  def compilar(path){
              println "(1)-------| Se clona el repositorio :" +path
              clonar();

              def jsonConstruccion = readFile(file:''+path)

              def dataConstruccion = new JsonSlurperClassic().parseText(jsonConstruccion)
              def rootDir = pwd()
              def email = load "${rootDir}@script/controlador/enviarCorreo.groovy"
              def construccionFallida = false

              if(env.Build == "Nan"){
                      try{
                              println "(2)-------| Se construye el Artefacto"


                              if (!dataConstruccion.cambio.construccion) {

                                      println "* Construcción del Artefacto"

                                      //Inicia la compilación
                                      println "Ruta : ${rootDir}"
                                      println "Valor del Branch"+env.gitlabSourceBranch

                                  switch(env.gitlabSourceBranch) {
                                    case 'Test':
                                    println "---------| Test |-------"
                                       sh " cd ${rootDir}/wsBancaDigital/ && gradle clean build"
                                       dataConstruccion.cambio.construccion = true
                                       println "-----| Se ha creado el artefacto SNAPSHOT |---"
                                    break
                                    case 'QA':
                                    println "---------| QA |-------"
                                      sh " cd ${rootDir}/wsBancaDigital/ && gradle build -Penv=release -Pqa=true --refresh-dependencies "
                                       println "-----| Se ha creado el artefacto RC |---"
                                      sh "cd ${rootDir}/wsBancaDigital/ && gradle build -Penv=release --refresh-dependencies"
                                        println "-----| Se ha creado el artefacto Release |---"

                                     dataConstruccion.cambio.construccion = true
                                   break

                                  default:

                                  break
                                }
                                      println "---- ¡ Construcción del Artefacto satisfactoria ! ---"
                                      println "---- OBTENIENDO NOMBRE DEL ARTEFACTO ----"
                                      def archivo = "No disponible"


                                      switch(gitlabSourceBranch) {
                                        case 'Test':
                                          archivo = sh returnStdout: true, script: 'cd wsBancaDigital/build/libs/ && ls *-SNAPSHOT.war'
                                        break

                                        case 'QA':
                                          archivo = sh returnStdout: true, script: 'cd wsBancaDigital/build/libs/ && ls *-RC.war'
                                        break
                                      }


                                      println "Nombre del Artefacto Generado: ${archivo}"
                                      dataConstruccion.cambio.nombre_artefacto = archivo.trim()
                                      println "---- LISTADO CORRECTO DEL DIRECTORIO WS BANCA DIGITAL ----"


                              } else if (dataConstruccion.cambio.construccion) {
                                      println "* La construcción ya existe"
                                      writeFile(file:''+path, text: jsonJenkinsFile)
                              }
                      }
                      catch(e) {
                          construccionFallida = true
                          println "Excepción|------ Error en la  Construcción"
                          println e.getMessage()

                      } finally {
                              println "(3)------| Envio de correo Exitoso"
                              println "Estado construccion " + construccionFallida

                              env.gitlabSourceBranch?email.envioEmail('construccion',construccionFallida):""

                              //si la construccion fue exitosa envia para autorizar el despliegue
                              println "envia correo para autorizar"
                              env.gitlabSourceBranch=='QA'?email.envioEmail('sendAutoriza',construccionFallida):""
                               //Escribiendo en Json
                               jsonConstruccion = JsonOutput.toJson(dataConstruccion)?:null
                               jsonConstruccion = JsonOutput.prettyPrint(jsonConstruccion)?:null
                               //Colocando el cambio en el archivo
                               writeFile(file:''+path, text: jsonConstruccion)?:null

                      }
              } else{
                      println "* La construcción ya existe"
              }


    }

return this

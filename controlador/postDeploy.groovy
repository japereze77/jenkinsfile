/*
@Creado: José Arturo Pérez
@version: 1.0
*/


def enviarCambioJson(){
  println "******************************* | PostDeploy | ********************************************"
  def construccionFallida = false
  try {
    switch (env.gitlabSourceBranch){
      case 'Test':
        return true
      break;
      case 'QA' :
        return true
      break;
      case 'master' :
        println "(1)----| Ejecutando el servicio Soap"
        def jsonFile = readFile  "wsBancaDigitalJson_${BUILD_NUMBER}/wsBancaDigitalJSon.json"
        jsonFile = jsonFile.replaceAll("\n", "");
        sh "java -jar ${pwd()}@script/serviceSoap/TestConsumeSoap.jar ${jsonFile} > soapResult"
        def soapResult = readFile 'soapResult'
        sh "rm -rf soapResult"

        println "(2)----| Servicio soap ejecutado Correctamente : "
      break;
    }

  }catch(e){
     println e.getMessage()
     construccionFallida = true
  } finally{
      def email = load "${pwd()}@script/controlador/enviarCorreo.groovy"
      env.gitlabSourceBranch=='master'?email.envioEmail('cambiosJson',construccionFallida):""
  }
}


return this

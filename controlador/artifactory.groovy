import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic

/*
@Creado: José Arturo Pérez
@version: 1.0
*/

def orquestadorArtifactory(evento,path) {

  try {

    println "RAMA ORIGEN:"
    println gitlabSourceBranch

    switch(gitlabSourceBranch) {
      case 'Test':
        if(evento == 'envio') {
            println "STAGE(Test) -> Rama TEST. Enviando artefacto a Repo SNAPSHOT "
            envioArtifactory("snapshot-bdm-cicd-test","*-SNAPSHOT.war",path)
        }

      break
      case 'QA':
        //def repoArt = 'release-bdm-cicd-qa'
        def repoArt = 'pipeline-test'
        if(evento == 'envio') {
            println "STAGE(Test) -> Rama QA. Enviando artefacto a Repo RC"
            envioArtifactory(repoArt,"*-RC.war",path)

        } else if(evento == 'descarga') {
            println "STAGE(Test) -> Rama QA. Descargando artefacto de Repo RC "
            descargaArtifactory(repoArt)
        }

      break
    }

  } catch(e) {
    println "ERROR AL EJECUTAR ORQUESTADOR DE ARTIFACTORY"
    println e
  } finally {
    println "TERMINA EJECUCION DE ORQUESTADOR ARTIFACTORY"
  }

}



def envioArtifactory(proyecto,extension,path) {



  println "******************************* | Artifactory | ********************************************"

  try{

    def artifactoryUrl = "http://10.50.109.61:8081/artifactory"

    def serverArtifactory = Artifactory.newServer url: 'http://10.50.109.61:8081/artifactory', credentialsId: 'artifactoryPipeline'
    serverArtifactory.connection.timeout = 120
    //serverArtifactory.bypassProxy = true

    println "Conexion establecida con artifactory"

    def rootDir = pwd()

    println "========= Entrando a directorio del WAR ========="

    def uploadSpec = """{
     "files": [
      {
          "pattern": "${rootDir}/wsBancaDigital/build/libs/${extension}",
          "target": "${proyecto}/"
        }
     ]
    }"""
    serverArtifactory.upload(uploadSpec)
    //def buildInfo = serverArtifactory.upload uploadSpec

    println "Envio a Artifactory realizado con Exito"


    // GENERAR URL DE DESCARGA DEL ARTEFACTO
    println "===== INFORMACIÓN DEL ENVIO ====="
    def jsonConstruccion = readFile(file:''+path)
    def dataConstruccion = new JsonSlurperClassic().parseText(jsonConstruccion)

    def urlDeploy = artifactoryUrl + "/" + proyecto + "/" + dataConstruccion.cambio.nombre_artefacto


    println "Directorio actual"
    sh "pwd"
    println "Direccion del PATH: ${path}"



    dataConstruccion.cambio.url_artefacto = urlDeploy
    jsonConstruccion = JsonOutput.toJson(dataConstruccion)?:null
    jsonConstruccion = JsonOutput.prettyPrint(jsonConstruccion)?:null
    writeFile(file:''+path, text: jsonConstruccion)?:null

    println "URL FINAL: ${urlDeploy}"
    //sh "cat ${path}"


    /*
    def email = load "${rootDir}@script/enviarCorreo.groovy"
    email.envioEmail('desployTest',false)
    */

  } catch(e){
    println "Excepcion|-----ERROR AL ENVIAR ARTEFACTO A ARTIFACTORY"
    println e
  } finally{


  }

}

def descargaArtifactory(directorio) {

  try{
      println "******************************* | Descarga de Artefacto | ********************************************"
      def Carpeta = fileExists 'Artefactos'

      if (Carpeta) {
          echo 'Ya existe la Carpeta'
        } else {
          echo 'No existe la Carpeta... Creandola...'; sh "mkdir -p Artefactos";
          }


    //def serverArtifactory = Artifactory.server 'artifactoryPipeline'
    def serverArtifactory = Artifactory.newServer url: 'http://10.50.109.61:8081/artifactory', credentialsId: 'artifactoryPipeline'
    serverArtifactory.connection.timeout = 60

    /** OBTENER EL NOMBRE DEL ARTEFACTO A DESCARGAR **/
    //

    def downloadSpec = """{
     "files": [
      {
          "pattern": "${directorio}/*.war",
          "target": "Artefactos/"
        }
     ]
    }"""
    serverArtifactory.download(downloadSpec)
    println "Descarga de ARTEFACTO realizada con Exito"
  } catch(e){
    println "Error al descargar el artefacto"
    println e
  } finally{

  }

}


return this

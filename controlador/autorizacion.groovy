import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
/*
@Creado: José Arturo Pérez
@version: 1.0
@fecha
*/
 def autorizacion(path){
      println "******************************* | Autorización | ******************************"

               if(env.Build != "Nan"){
                   //Se lee el archivo json con los parametros solicitados ...
                   def pathjson = "wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json"
                   println "pathjson = "+pathjson
                   def jsonAutorizacion = readFile(file:''+pathjson)
                   def dataAutorizacion = new JsonSlurperClassic().parseText(jsonAutorizacion)
                   def email = load "${pwd()}@script/enviarCorreo.groovy"
                                   try{
                                           if (!dataAutorizacion.cambio.autorizacionQA) {

                                                   println "* Autorizacion registrada"
                                                   dataAutorizacion.cambio.autorizacionQA = true
                                                   dataAutorizacion.cambio.autorizacionQAEnviada = true
                                           }
                                           else if (dataAutorizacion.cambio.autorizacionQA) {
                                                println "* La Autorización QA ya fue ejecutada."
                                                email.envioEmail('autorizacionFallida',false)
                                                return "5"
                                           }
                                           else if (!dataAutorizacion.cambio.autorizacionQAEnviada) {
                                                        println "* La Autorización no ha sido enviada."
                                                        email.envioEmail('autorizacionFallida',false)
                                                        return "5"
                                            }
                                    }
                                  catch(e) {

                                           def rootDir = pwd()
                                           println e.getMessage()
                                           //en caso de alguna excepcion retorno un 3
                                           return "3"
                                   } finally {

                                           //Escribiendo en Json
                                           jsonAutorizacion = JsonOutput.toJson(dataAutorizacion)?:null
                                           jsonAutorizacion = JsonOutput.prettyPrint(jsonAutorizacion)?:null
                                           //Colocando el cambio en el archivo
                                           //writeFile(file:'wsBancaDigitialQAJson_'+${BUILD_NUMBER}+'/wsBancaDigitalQAJSon.json', text: jsonAutorizacion)?:null
                                           writeFile(file:''+pathjson, text: jsonAutorizacion)?:null
                                   }
           }else{
                   println "* La Autorización ha sido enviada."
          }
 }

return this

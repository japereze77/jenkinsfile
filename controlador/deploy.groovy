import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import static groovy.io.FileType.FILES

/*
@Creado: José Arturo Pérez
@version: 1.0
*/




def ejecutaRest(metodo, ipServidor, JsonModificar, ambiente){

	println "Se ejecuta el ejecutaRest"

	def IP = ipServidor.toString()

    def response = httpRequest acceptType: 'APPLICATION_JSON', consoleLogResponseBody: true, contentType: 'APPLICATION_JSON', httpMode: 'POST', requestBody: '{"IP":"'+ipServidor+'"}', responseHandle: 'NONE', url: "http://10.63.32.116:98/BalancerAdminBazDig/"+metodo

	def jsonEstado = readFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json")
	def dataEstado = new JsonSlurperClassic().parseText(jsonEstado)

	dataEstado.cambio."$ambiente"[0][JsonModificar] = response.getContent()

	jsonEstado = JsonOutput.toJson(dataEstado)?:null
    jsonEstado = JsonOutput.prettyPrint(jsonEstado)?:null
	writeFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json", text: jsonEstado)?:null

}

def calculoIPAmbiente(){

	println "Se ejecuta el calculoIPAmbiente"



		println "Si se ejecuta Build diferente"
		if(env.gitlabSourceBranch == "Test"){
			env.ambiente = "test"

			def jsonEstado = readFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json")
			def dataEstado = new JsonSlurperClassic().parseText(jsonEstado)

			def IP = dataEstado.cambio.test.IP_TEST_10_63_32_179

			IP=IP.toString()

			IP= IP.replace("[","").replace("]","")

			estadoBalanceo(IP, env.ambiente)
		}
	if(env.Build != "Nan"){
		 if(env.ambiente.equals("QA")){

			println "El ambiente es QA"

			def jsonEstado = readFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json")
			def dataEstado = new JsonSlurperClassic().parseText(jsonEstado)

			def IP = dataEstado.cambio.QA.IP_QA_10_63_32_180

			IP=IP.toString()

			IP= IP.replace("[","").replace("]","")

			estadoBalanceo(IP, env.ambiente)

		}else{

			println "No hay Ambiente"

		}

	}else{

		println "Es una nueva Compilacion"

	}

}


	def estadoBalanceo(IP, ambiente){

		println "Se ejecuta el estadoBalanceo"

		def jsonEstado = readFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json")
    	def dataEstado = new JsonSlurperClassic().parseText(jsonEstado)
			println "si"

			/*def jsonEstado = readFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json")

			*/
			def Estado1 = "Estado_"+IP.replace(".","_")
			def Meter1 = "Meter_"+IP.replace(".","_")
			def Sacar1 = "Sacar_"+IP.replace(".","_")
			def Estatus_Deploy1 = "Estatus_Deploy_"+IP.replace(".","_")

			def varEstadoBalanceo = dataEstado.cambio."$ambiente"."$Estado1"
			def varMeterBalanceo = dataEstado.cambio."$ambiente"."$Meter1"
			def varSacarBalanceo = dataEstado.cambio."$ambiente"."$Sacar1"
			def varEstatus_Deploy = dataEstado.cambio."$ambiente"."$Estatus_Deploy1"

			varEstadoBalanceo=varEstadoBalanceo.toString()
			varMeterBalanceo=varMeterBalanceo.toString()
			varSacarBalanceo=varSacarBalanceo.toString()
			varEstatus_Deploy=varEstatus_Deploy.toString()

			varEstadoBalanceo= varEstadoBalanceo.replace("[","").replace("]","").replace("{", "").replace("}", "")[-2..-1].replace(":", "")
			varMeterBalanceo= varMeterBalanceo.replace("[","").replace("]","").replace("{", "").replace("}", "")[-2..-1].replace(":", "")
			varSacarBalanceo= varSacarBalanceo.replace("[","").replace("]","").replace("{", "").replace("}", "")[-2..-1].replace(":", "")
			varEstatus_Deploy= varEstatus_Deploy.replace("[","").replace("]","")

			println "Estado = "+ varEstadoBalanceo
			println "Meter = "+ varMeterBalanceo
			println "Sacar = "+ varSacarBalanceo
			println "Estatus Deploy = "+ varEstatus_Deploy

			println "IP = "+ IP
			println "Ambiente = "+ ambiente


			if(varEstadoBalanceo.equals("00")){

				println "Ejecutando estadoBalanceo por Estado = 00"

				def metodo = 'GetStatus'
				def ipServidor = IP
				def JsonModificar = Estado1

				ejecutaRest(metodo, ipServidor, JsonModificar, ambiente)

				Thread.sleep (10000)

				estadoBalanceo(IP, ambiente)


				}else if (varEstadoBalanceo.equals("1")) {

					println"Se deploya desde enviarSSHDeploy por Estado = 1"

					def JsonModificar = Estatus_Deploy1
					dataEstado.cambio."$ambiente"[0][Estado1] = "AA"
					jsonEstado = JsonOutput.toJson(dataEstado)?:null
					jsonEstado = JsonOutput.prettyPrint(jsonEstado)?:null
					writeFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json", text: jsonEstado)?:null

					enviarSSHDeploy(ambiente, JsonModificar, IP)

					}else if (varEstadoBalanceo.equals("-1")) {

						println "Ejecutando estadoBalanceo por Estado = -1"

						def metodo = 'GetStatus'
						def ipServidor = IP
						def JsonModificar = Estado1

						ejecutaRest(metodo, ipServidor, JsonModificar, ambiente)

						Thread.sleep (10000)

						estadoBalanceo(IP, ambiente)

						}else if (varEstadoBalanceo.equals("0")) {

							println "Ejecutando estadoBalanceo por Estado = 0 y Sacar = 00"

							if(varSacarBalanceo.equals("00")){

								def metodo = 'OutBalancer'
								def ipServidor = IP
								def JsonModificar = Sacar1

								ejecutaRest(metodo, ipServidor, JsonModificar, ambiente)

								Thread.sleep (10000)

								estadoBalanceo(IP, ambiente)

								}else if(varSacarBalanceo.equals("-1")){

									def metodo = 'OutBalancer'
									def ipServidor = IP
									def JsonModificar = Sacar1

									ejecutaRest(metodo, ipServidor, JsonModificar, ambiente)

									Thread.sleep (10000)

									estadoBalanceo(IP, ambiente)

									}else if(varSacarBalanceo.equals("0")){

										println"Se deploya desde enviarSSHDeploy por Estado = 0 y Sacar = 0"

										def JsonModificar = Estatus_Deploy1
										dataEstado.cambio."$ambiente"[0][Estado1] = "AA"
										jsonEstado = JsonOutput.toJson(dataEstado)?:null
										jsonEstado = JsonOutput.prettyPrint(jsonEstado)?:null
										writeFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json", text: jsonEstado)?:null

										enviarSSHDeploy(ambiente, JsonModificar, IP)
									}

								}

								if(varEstatus_Deploy.equals("TRUE")){

									println "Ejecutando estadoBalanceo por Estatus Deploy = TRUE"
									dataEstado.cambio."$ambiente"[0][Estatus_Deploy1] = "FALSE"

									jsonEstado = JsonOutput.toJson(dataEstado)?:null
									jsonEstado = JsonOutput.prettyPrint(jsonEstado)?:null
									writeFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json", text: jsonEstado)?:null

									def metodo = 'InBalancer'
									def ipServidor = IP
									def JsonModificar = Meter1

									ejecutaRest(metodo, ipServidor, JsonModificar, ambiente)

									Thread.sleep (10000)

									estadoBalanceo(IP, ambiente)

								}

								if(varEstadoBalanceo.equals("AA") && varMeterBalanceo.equals("0")){

									println "Se ingreso correctamente al Balanceo"

									dataEstado.cambio."$ambiente"[0][Meter1] = varMeterBalanceo

									jsonEstado = JsonOutput.toJson(dataEstado)?:null
									jsonEstado = JsonOutput.prettyPrint(jsonEstado)?:null
									writeFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json", text: jsonEstado)?:null

									}else if(varEstadoBalanceo.equals("AA") && varMeterBalanceo.equals("-1")){

										println "Se ingreso incorrectamente al Balanceo"
										println "Ejecutando estadoBalanceo por Estado = AA y Meter = -1"

										def metodo = 'InBalancer'
										def ipServidor = IP
										def JsonModificar = Meter1

										ejecutaRest(metodo, ipServidor, JsonModificar, ambiente)

										Thread.sleep (10000)

										estadoBalanceo(IP, ambiente)

									}

								}

  def enviarSSHDeploy(ambiente, JsonModificar, IP){
    println"******************** Deploy ***************************"
    def jbossAmbiente
    def jbossDepoy
    def versionArtefacto
    def rutaPrefijo
    def dirDeploy
    def credenciales
    def rootDir = pwd()
    def email = load "${rootDir}@script/controlador/enviarCorreo.groovy"
    def construccionFallida = false

    println '------| Deploy '+ env.gitlabSourceBranch +' |------'

     try{

          switch(env.gitlabSourceBranch) {

            case 'Test':

              rutaPrefijo =   "wsBancaDigital/build/libs/"
              versionArtefacto  = "${rutaPrefijo}*-SNAPSHOT.war"
              dirDeploy = '/home/jboss/jboss-eap-6.4/bancadigital/deployments'
              credenciales = 'corebazdigdes'

              if(!params.reverso)
                {
                  jbossAmbiente = 'cd /home/jboss/jboss-eap-6.4/bancadigital  && ./Respaldar.sh && rm -rf  /home/jboss/jboss-eap-6.4/bancadigital/deployments/*'
                  jbossDepoy = '''cd /home/jboss/jboss-eap-6.4/bancadigital && kill -9 `ps -fea|grep bancadigital|grep -v "grep"|awk {\'print $2\'}` && echo \'PROCESO ELIMINADO POR JENKINS\' && ./Reiniciar.sh'''

                }else{
                  jbossAmbiente = 'rm -rf  /home/jboss/jboss-eap-6.4/bancadigital/deployments/*'
                  jbossDepoy = '''cd /home/jboss/jboss-eap-6.4/bancadigital && kill -9 `ps -fea|grep bancadigital|grep -v "grep"|awk {\'print $2\'}` && echo \'PROCESO ELIMINADO POR JENKINS\' && ./Reversar.sh'''
                }

            break

            case 'QA':

              rutaPrefijo =   "wsBancaDigital/build/libs/"
              versionArtefacto  = "${rutaPrefijo}*-RC.war"
              dirDeploy = '/home/jboss/jboss-eap-6.4/bancadigital/deployments'
              credenciales = 'jbossQA'

              if(!params.reverso)
                {
                  jbossAmbiente = 'cd /home/jboss/jboss-eap-6.4/bancadigital  && ./Respaldar.sh && rm -rf  /home/jboss/jboss-eap-6.4/bancadigital/deployments/*'
                  jbossDepoy = '''cd /home/jboss/jboss-eap-6.4/bancadigital && kill -9 `ps -fea|grep bancadigital|grep -v "grep"|awk {\'print $2\'}` && echo \'PROCESO ELIMINADO POR JENKINS\' && ./Reiniciar_bancadigital.sh'''

                  }else{
                    jbossAmbiente = 'rm -rf  /home/jboss/jboss-eap-6.4/bancadigital/deployments/*'
                    jbossDepoy = '''cd /home/jboss/jboss-eap-6.4/bancadigital && kill -9 `ps -fea|grep bancadigital|grep -v "grep"|awk {\'print $2\'}` && echo \'PROCESO ELIMINADO POR JENKINS\' && ./Reverso.sh'''
                  }

            break
          }

          println "(1)----| Eliminación de Artefactos anteriores"
          sshPublisher(publishers: [sshPublisherDesc(configName: credenciales, transfers: [sshTransfer(cleanRemote: false, excludes: '', execCommand: jbossAmbiente, execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: '', remoteDirectorySDF: false, removePrefix: '', sourceFiles: '')], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false)])
          println "(2)----|  Realizando el Deploy"
          sshPublisher(publishers: [sshPublisherDesc(configName: credenciales,transfers: [sshTransfer(excludes: '', execCommand: jbossDepoy, execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: dirDeploy, remoteDirectorySDF: false, removePrefix: rutaPrefijo, sourceFiles: versionArtefacto )], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false)])
          println "(3)----| Deploy Exitoso!"
          construccionFallida = false
// -----------------------------------------------Uso del Saca Apps------------------------------------------------------
					def jsonEstado = readFile(file:"wsBancaDigitalJson_${env.Build }/wsBancaDigitalJSon.json")
					def dataEstado = new JsonSlurperClassic().parseText(jsonEstado)

					dataEstado.cambio."$ambiente"[0][JsonModificar] = "TRUE"

					jsonEstado = JsonOutput.toJson(dataEstado)?:null
						jsonEstado = JsonOutput.prettyPrint(jsonEstado)?:null
					writeFile(file:"wsBancaDigitalJson_${env.Build}/wsBancaDigitalJSon.json", text: jsonEstado)?:null

					estadoBalanceo(IP, ambiente)


    }catch(e){
      construccionFallida = true
      println "Excepción|------ Error al Realizar el Deploy"
      e.getMessage();

    }finally{
      println "(5)----| Correo Deploy Exitoso!"
      println "Estado Deploy Fallido : " + construccionFallida
      email.envioEmail('desployTest',construccionFallida)
    }

  }

   def status(path){
        def rootDirGradle = pwd()
        def jsonConstruccion = readFile(file:''+path)
        def dataConstruccion = new JsonSlurperClassic().parseText(jsonConstruccion)
        def dir = new File("${rootDirGradle}");
        def status
                        dir.traverse(type: FILES , nameFilter: ~/.*\.dodeploy/)         { status = "dodeploy"      };
                        dir.traverse(type: FILES , nameFilter: ~/.*\.skipdeploy/)       { status = "skipdeploy"    };
                        dir.traverse(type: FILES , nameFilter: ~/.*\.isdeploying/)      { status = "isdeploying"   };
                        dir.traverse(type: FILES , nameFilter: ~/.*\.deployed/)         { status = "deployed"      };
                        dir.traverse(type: FILES , nameFilter: ~/.*\.failed/)           { status = "failed"        };
                        dir.traverse(type: FILES , nameFilter: ~/.*\.isundeploying/)    { status = "isundeploying" };
                        dir.traverse(type: FILES , nameFilter: ~/.*\.undeployed/)       { status = "undeployed"    };
                        dir.traverse(type: FILES , nameFilter: ~/.*\.pending/)          { status = "pending"       };
         dataConstruccion.cambio.status = status
         jsonConstruccion = JsonOutput.toJson(dataConstruccion)?:null
         jsonConstruccion = JsonOutput.prettyPrint(jsonConstruccion)?:null
         writeFile(file:''+path, text: jsonConstruccion)?:null
         println "******************************* | Estatus del Deploy  | ********************************************"
         println "Estatus del deploy : " + status
 }



  return this

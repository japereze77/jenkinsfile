import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import java.io.*;
import java.io.*;
import java.net.*;
/*
@Creado: José Arturo Pérez
@version: 1.0
*/

    def ambienteJson(path){

                  println("*******************************| Ambiente - ${env.gitlabSourceBranch}|******************************")


                  println "(1) ------- | Se conecta a los datos json"
                  def jsonJenkinsFile
                  def dataJenkinsFile
                  def rootDir = pwd()


                                  try{
                                          println "Creando repositorio wsBancaDigitalJson_${BUILD_NUMBER}"
                                          sh "mkdir wsBancaDigitalJson_${BUILD_NUMBER}"
                                          println "CARPETA DE PROYECTO CREADA. LISTANDO DIRECTORIO"
                                          sh "cp ${pwd()}@script/wsBancaDigitalJSon.json ${pwd()}/wsBancaDigitalJson_${BUILD_NUMBER}"
                                          println "Path: "+path
                                          jsonJenkinsFile = readFile(file:''+ path)
                                          println "(4)-------| Se registra el cambio en el json"

                                          dataJenkinsFile = new JsonSlurperClassic().parseText(jsonJenkinsFile)
                                          dataJenkinsFile.cambio.numero_construccion = "${BUILD_NUMBER}"
                                          //agrego id_construccion
                                          //dataJenkinsFile.cambio.id_construccion = "${BUILD_NUMBER}"
                                          //dataJenkinsFile.cambio.nombre_artefacto = "NombreArtefacto"
                                          dataJenkinsFile.cambio.descarga_jenkinsfile = true

                                          } catch (ex) {
                                                  println "Excepcion|-----Error en Ambiente : "+ex.getMessage()
                                                  return "1"
                                          } finally {

                                          println "(5)-------| Clonado Proyecto Git Exitso"

                                             //Escribiendo en Json
                                             jsonJenkinsFile = JsonOutput.toJson(dataJenkinsFile)?:null
                                             jsonJenkinsFile = JsonOutput.prettyPrint(jsonJenkinsFile)?:null
                                             //Colocando el cambio en el archivo
                                             writeFile(file:''+path, text: jsonJenkinsFile)
					                         //writeFile(file:'wsBancaDigitialJson_'+${BUILD_NUMBER}+'/wsBancaDigitalJSon.json', text: jsonJenkinsFile)?:null
                                          }



                      }

return this
